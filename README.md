# Waycheck

Waycheck is a simple Qt6 application that displays all of the Wayland protocols that your compositor supports, and all of the protocols that it doesn't support. It can be used to compare protocol support between compositors, or if you're working on your own compositor, to keep track of which protocols you still need to implement.

## Installation

### Distro Packages

[![Distribution packaging status](https://repology.org/badge/vertical-allrepos/waycheck.svg)](https://repology.org/project/waycheck/versions)

### Flatpak

[![Download on Flathub](https://dl.flathub.org/assets/badges/flathub-badge-en.svg)](https://flathub.org/apps/dev.serebit.Waycheck)

## License

Waycheck is licensed under the `Apache-2.0` open-source license.

The logomark is inspired by the Wayland logomark, and was co-designed by Campbell Jones (@serebit) and Oliver Beard (@olib). It is dual-licensed under the terms of the `CC0-1.0` and `Apache-2.0` open-source licenses.
