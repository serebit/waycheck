#!/usr/bin/env sh

METAINFO_VERSION=$(grep "release version=" resources/dev.serebit.Waycheck.metainfo.xml | head -n1 | cut -d'"' -f2)
METAINFO_URL_VERSION=$(grep "waycheck/-/releases" resources/dev.serebit.Waycheck.metainfo.xml | head -n1 | cut -d'/' -f8 | cut -d'<' -f1)
MESON_VERSION=$(grep "version:" meson.build | head -n1 | cut -d"'" -f2)

if [ "$METAINFO_VERSION" != "$MESON_VERSION" ]; then
  exit 1
fi

if [ "$METAINFO_URL_VERSION" != "v$MESON_VERSION" ]; then
  exit 2
fi
