#!/usr/bin/env sh

set -e

GIT_ROOT=$(git rev-parse --show-toplevel)

rm -rf "$GIT_ROOT/signatures"
mkdir -p "$GIT_ROOT/signatures/gitlab"
mkdir -p "$GIT_ROOT/signatures/codeberg"
echo "*" > "$GIT_ROOT/signatures/.gitignore"

VERSION=$(grep "version:" meson.build | head -n1 | cut -d"'" -f2)

GITLAB_RELURL="https://gitlab.freedesktop.org/serebit/waycheck/-/archive/v${VERSION}"
GITLAB_ZIP="waycheck-v${VERSION}.zip"
GITLAB_GZTAR="waycheck-v${VERSION}.tar.gz"
GITLAB_BZTAR="waycheck-v${VERSION}.tar.bz2"
GITLAB_TAR="waycheck-v${VERSION}.tar"

for file in "$GITLAB_ZIP" "$GITLAB_GZTAR" "$GITLAB_BZTAR" "$GITLAB_TAR"; do
  wget -qN "$GITLAB_RELURL/$file" -O "$GIT_ROOT/signatures/gitlab/$file"
  rm -f "$GIT_ROOT/signatures/gitlab/${file}.asc" # just in case it already exists
  gpg --armor --detach-sign "$GIT_ROOT/signatures/gitlab/$file"
  gpg --verify "$GIT_ROOT/signatures/gitlab/${file}.asc"
  rm -f "$GIT_ROOT/signatures/gitlab/${file}"
done

CODEBERG_RELURL="https://codeberg.org/serebit/waycheck/archive"
CODEBERG_ZIP="v${VERSION}.zip"
CODEBERG_GZTAR="v${VERSION}.tar.gz"

for file in "$CODEBERG_ZIP" "$CODEBERG_GZTAR"; do
  wget -qN "$CODEBERG_RELURL/$file" -O "$GIT_ROOT/signatures/codeberg/$file"
  rm -f "$GIT_ROOT/signatures/codeberg/${file}.asc" # just in case it already exists
  gpg --armor --detach-sign "$GIT_ROOT/signatures/codeberg/$file"
  gpg --verify "$GIT_ROOT/signatures/codeberg/${file}.asc"
  rm -f "$GIT_ROOT/signatures/codeberg/${file}"
done
