#include "window.hpp"

#include <QApplication>
#include <QMessageBox>

int main(int argc, char** argv) {
    auto app = QApplication(argc, argv);

    QApplication::setDesktopFileName(QStringLiteral("dev.serebit.Waycheck"));

    const auto waylandApp = qApp->nativeInterface<QNativeInterface::QWaylandApplication>();
    if (waylandApp == nullptr) {
        QMessageBox msgBox;
        msgBox.setText("Waycheck must be started within a Wayland session.");
        return msgBox.exec();
    }

    auto window = Window(waylandApp);
    window.show();

    return QApplication::exec();
}
