#ifndef MODEL_HPP
#define MODEL_HPP

#include "protocols.hpp"

#include <QSortFilterProxyModel>
#include <QStandardItemModel>

class Model final : public QObject {
    Q_OBJECT

  public:
    enum SupportFilters {
        ALL,
        SUPPORTED,
        UNSUPPORTED
    };

  private:
    QSortFilterProxyModel searchModel;
    QSortFilterProxyModel supportModel;
    QString searchFilter;
    SupportFilters supportFilter;

  public:
    explicit Model(ProtocolSource source);
    ~Model() override;

    QStandardItemModel sourceModel;
    QSortFilterProxyModel* const filteredModel;
    QString const name;
    bool const includeStatus;
    bool const includeName;

    void setSearchFilter(const QString& newSearchFilter);

    void setSupportFilter(SupportFilters newSupportFilter);

    void updateFilter();

  signals:
    void filterChanged(bool filtering);
};

#endif
