#ifndef WINDOW_HPP
#define WINDOW_HPP

#include "model.hpp"
#include "protocols.hpp"

#include <QComboBox>
#include <QGuiApplication>
#include <QLineEdit>
#include <QMainWindow>
#include <QStyledItemDelegate>
#include <QTableView>

#include <functional>
#include <memory>

QT_BEGIN_NAMESPACE
namespace Ui {
class Window;
}
QT_END_NAMESPACE

class Window final : public QMainWindow {
    Q_OBJECT
    std::map<ProtocolSource, std::shared_ptr<Model>> models;
    ProtocolStore store;

  public:
    explicit Window(const QNativeInterface::QWaylandApplication* waylandApp, QWidget* parent = nullptr);
    ~Window() override;

    void newGlobal(const std::string& interface, uint32_t version);

    void addProtocol(const Protocol& protocol);

  private:
    void initTable(ProtocolSource source, QTableView& table);

  private:
    std::unique_ptr<Ui::Window> const ui;
    QLineEdit searchBox;
    QComboBox supportBox;
};

class TableItemDelegate final : public QStyledItemDelegate {
  public:
    explicit TableItemDelegate(QObject* parent = nullptr);

    void paint(QPainter* painter, const QStyleOptionViewItem& option, const QModelIndex& index) const override;
};

#endif
