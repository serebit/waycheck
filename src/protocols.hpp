#ifndef WAYCHECK_PROTOCOLS_HPP
#define WAYCHECK_PROTOCOLS_HPP

#include <optional>
#include <set>
#include <string>
#include <unordered_map>

enum ProtocolStatus {
    STABLE,
    STAGING,
    UNSTABLE,
    NONE,
};

enum ProtocolSource {
    UNKNOWN,
    UPSTREAM,
    WLROOTS,
    KDE,
    COSMIC,
    WESTON,
    CHROMEOS,
    TREELAND,
    FROG,
    GAMESCOPE,
};

struct Protocol {
    ProtocolSource source;
    ProtocolStatus status;
    std::string pretty;
    std::string name;
};

class ProtocolStore {
    std::unordered_map<std::string, const Protocol> known_protocols;
    std::unordered_map<std::string, const std::string> known_interfaces;

    void add(const Protocol& protocol, const std::set<std::string>& interfaces) noexcept;

  public:
    ProtocolStore() noexcept;

    std::optional<Protocol> protocolForInterface(const std::string& interface) const noexcept;
    std::set<const Protocol*> getProtocols() const noexcept;
};

std::string source_to_string(ProtocolSource source);
std::string status_to_string(ProtocolStatus status);

#endif
